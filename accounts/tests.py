from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import date
import unittest

# Create your tests here.

class Lab1UnitTest(TestCase):

    def test_lab_11_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_else_does_not_exist(self):
        response = Client().get('/antah-berantah/')
        self.assertEqual(response.status_code,404)

    def test_lab_11_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')


    